# IOTFINAL

Project IOTFinal - Door Sensor/Room Monitor

Description: DOOR SENSOR THAT WILL DECTECT WHEN THE TARGETED DOOR HAS BEEN OPEN. AS WELL AS TRACK
             MOTION TO SEE IF THE PERSON IS MOVING INTO THE ROOM OR LEAVING THE ROOM.

Author: Guan Ming Chee, Jacob Hughes, Joshua Schmitz

Date: 11.17.2019

Goal: To create a door sensor that will be attached to any chosen door and monitor trackflow in and out of the room. With this we can monitor various values such as if a room is being used, total number of people in room and time that room was entered. This project is also designed to send a notification to a mobile device to alert the user that the tracked door is opened using a magnetic door switch.